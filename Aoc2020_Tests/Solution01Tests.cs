﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aoc2020_Blazor.Solutions;
using Xunit;
using xUnitHelpers;

namespace Aoc2020_Tests
{
    public class Solution01Tests : ISolutionTests
    {
        private const int _day = 01;
        public int Day => _day;

        SolutionCommon solver = new Solution01();

        [Theory]
        [TxtFileData(@".\TestData\Day01\Day01_Scenario00_In.txt", @".\TestData\Day01\Day01_Scenario00_Out.txt")]
        [TxtFileData(@".\TestData\Day01\Day01_Scenario01_In.txt", @".\TestData\Day01\Day01_Scenario01_Out.txt")]
        // borrowed from https://github.com/encse/
        [TxtFileData(@".\TestData\Day01\Day01_Scenario02_In.txt", @".\TestData\Day01\Day01_Scenario02_Out.txt")]
        public void PartATest(string input, string expectedAOutput, string expectedBOutput)
        {
            /* Arrange */
            // Empty

            /* Act */
            string realOutput = solver.PartA(input);

            /* Assert */
            Assert.Equal(expected: expectedAOutput, actual: realOutput);
        }

        [Theory]
        [TxtFileData(@".\TestData\Day01\Day01_Scenario00_In.txt", @".\TestData\Day01\Day01_Scenario00_Out.txt")]
        [TxtFileData(@".\TestData\Day01\Day01_Scenario01_In.txt", @".\TestData\Day01\Day01_Scenario01_Out.txt")]
        // borrowed from https://github.com/encse/
        [TxtFileData(@".\TestData\Day01\Day01_Scenario02_In.txt", @".\TestData\Day01\Day01_Scenario02_Out.txt")]
        public void PartBTest(string input, string expectedAOutput, string expectedBOutput)
        {
            /* Arrange */
            // Empty

            /* Act */
            string realOutput = solver.PartB(input);

            /* Assert */
            Assert.Equal(expected: expectedBOutput, actual: realOutput);
        }
    }
}
