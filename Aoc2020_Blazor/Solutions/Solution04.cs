﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution04 : SolutionCommon
    {
        private List<string> requiredFields = new List<string>()
            { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };

        private Dictionary<string, string> validationPatterns = new Dictionary<string, string> {
            {"byr", @"^(19[2-9][0-9]|20[0][0-2])$"},
            {"ecl", @"^(amb|blu|brn|gry|grn|hzl|oth)$"},
            {"eyr", @"^(202[0-9]|2030)$" },
            {"hcl", @"^(#[0-9a-f]{6})$"},
            {"hgt", @"^(1[5-8][0-9]cm|19[0-3]cm|59in|6[0-9]in|7[0-9]in)$"},
            {"iyr", @"^(201[0-9]|2020)$"},
            {"pid", @"^([0-9]{9})$"}
        };

        private Dictionary<string, string> PopulatePassportFields(string passportLine)
        {
            Dictionary<string, string> passport = new Dictionary<string, string>();

            string pattern = @"(\b\S+):(\S+\b)";
            var matches = Regex.Matches(passportLine, pattern);

            // If we use anonymous var here, then match will be of type `object`!
            foreach (Match match in matches) 
            {
                string key = match.Groups[1].Value;
                string value = match.Groups[2].Value;
                passport.Add(key, value);
            }

            return passport;
        }


        public override string PartA(string input)
        {
            int validPassportCounter = 0;

            // Each split on double newline is a "raw passport" of a single person
            var rawPassports = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

            foreach (var rawPassport in rawPassports)
            {
                var passport = PopulatePassportFields(rawPassport);

                bool isValid = true;
                foreach (string requiredField in requiredFields)
                {
                    if (passport.ContainsKey(requiredField) == false)
                    {
                        isValid = false;
                        break;
                    }
                }

                if (isValid)
                {
                    validPassportCounter++;
                }
            }

            return validPassportCounter.ToString();
        }

        public override string PartB(string input)
        {
            int validPassportCounter = 0;

            // Each split on double newline is a "raw passport" of a single person
            var rawPassports = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

            foreach (var rawPassport in rawPassports)
            {
                var passport = PopulatePassportFields(rawPassport);

                bool isValid = true;
                foreach (string requiredField in requiredFields)
                {
                    if (passport.ContainsKey(requiredField) == false)
                    {
                        isValid = false;
                        break;
                    }

                    Regex pattern = new Regex(validationPatterns[requiredField]);
                    if (pattern.IsMatch(passport[requiredField]) == false)
                    {
                        isValid = false;
                        break;
                    }
                }

                if (isValid)
                {
                    validPassportCounter++;
                }
            }

            return validPassportCounter.ToString();
        }
    }
}
