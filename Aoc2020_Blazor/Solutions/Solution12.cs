﻿using System;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution12 : SolutionCommon
    {
        // Labels so that it's harder to make a simple typo
        internal const char Up = 'N';
        internal const char Down = 'S';
        internal const char Left = 'W';
        internal const char Right = 'E';
        internal const char Forward = 'F';
        internal const char RotCounterClockwise = 'L';
        internal const char RotClockwise = 'R';

        internal const int InitialWaypointX = 10;
        internal const int InitialWaypointY = 1;

        private class Ship
        {
            // There *could* be private fields that those properties expose
            // but I don't care enough to implement them here
            public int PositionX { get; private set; }
            public int PositionY { get; private set; }
            public int Bearing { get; private set; }

            // Waypoint is required only for Part B
            public int WaypointX { get; private set; }
            public int WaypointY { get; private set; }

            // This is the only contructor that is required by the problem
            // We only care about relative ship movement, so we may just
            // assume that the ship starting point (x,y) is (0,0)
            public Ship()
            {
                PositionX = 0;
                PositionY = 0;
                Bearing = 0;
                WaypointX = InitialWaypointX;
                WaypointY = InitialWaypointY;
            }

            public void MoveShipDirectly(string movement)
            {
                char action = movement[0];
                int offset = Int32.Parse(movement[1..]);

                switch (action)
                {
                    case Up:
                        PositionY += offset; break;
                    case Down:
                        PositionY -= offset; break;
                    case Right:
                        PositionX += offset; break;
                    case Left:
                        PositionX -= offset; break;
                    case RotCounterClockwise:
                        Bearing += offset;
                        Bearing = (Bearing + 360) % 360; // Wrap the Bearing to {0,90,180,270}
                        break;
                    case RotClockwise:
                        Bearing -= offset;
                        Bearing = (Bearing + 360) % 360; // Wrap the Bearing to {0,90,180,270}
                        break;
                    case Forward:
                        switch (Bearing)
                        {
                            case 0:
                                PositionX += offset; break;
                            case 90:
                                PositionY += offset; break;
                            case 180:
                                PositionX -= offset; break;
                            case 270:
                                PositionY -= offset; break;
                            default:
                                throw new ApplicationException("Invalid bearing value!");
                        }
                        break;
                    default:
                        throw new ApplicationException("Invalid action character!");
                }
            }

            public void MoveShipOrWaypoint(string movement)
            {
                char action = movement[0];
                int offset = Int32.Parse(movement[1..]);

                switch (action)
                {
                    case Up:
                        WaypointY += offset; break;
                    case Down:
                        WaypointY -= offset; break;
                    case Right:
                        WaypointX += offset; break;
                    case Left:
                        WaypointX -= offset; break;
                    case RotCounterClockwise:
                        (WaypointX, WaypointY) = WaypointRotate(offset, WaypointX, WaypointY);
                        break;
                    case RotClockwise:
                        // Clockwise rotation over x angle <=> Counterclockwise rotation over -x angle
                        (WaypointX, WaypointY) = WaypointRotate(-offset, WaypointX, WaypointY);
                        break;
                    case Forward:
                        PositionX += WaypointX * offset;
                        PositionY += WaypointY * offset;
                        break;
                    default:
                        throw new ApplicationException("Invalid action character!");
                }

                // Local helper to calculate *counterclockwise* waypoint rotation
                (int x, int y) WaypointRotate(int rotAngle, int waypointX, int waypointY)
                {
                    int newX = waypointX;
                    int newY = waypointY;

                    /*
                    * Consider the following formula for 2D vector rotation:
                    * 
                    * [rotated_x]   [cos(angle)  -sin(angle)]   [x]
                    * [rotated_y] = [sin(angle)   cos(angle)] * [y]
                    * 
                    * For angle = 90, the rotation matrix is simply
                    * [0  -1]
                    * [1   0]
                    * so rotated_x = -y, and rotated_y = x
                    * 
                    * We easily can "precompute" the outcomes for each valid rotation angle,
                    * i.e. 0, 90, 180 and 270.
                    */

                    int rotAngleWrapped = ((rotAngle + 360) % 360);

                    return rotAngleWrapped switch
                    {
                        0 => (waypointX, waypointY),
                        90 => (-waypointY, waypointX),
                        180 => (-waypointX, -waypointY),
                        270 => (waypointY, -waypointX),
                        _ => throw new ApplicationException("Invalid rotation angle!")
                    };
                }
            }

        }

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var ferry = new Ship();

            foreach (var movement in inputLines)
            {
                ferry.MoveShipDirectly(movement);
            }

            int distance = Math.Abs(ferry.PositionX) + Math.Abs(ferry.PositionY);

            return distance.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var ferry = new Ship();

            foreach (var movement in inputLines)
            {
                ferry.MoveShipOrWaypoint(movement);
            }

            int distance = Math.Abs(ferry.PositionX) + Math.Abs(ferry.PositionY);

            return distance.ToString();
        }
    }
}
