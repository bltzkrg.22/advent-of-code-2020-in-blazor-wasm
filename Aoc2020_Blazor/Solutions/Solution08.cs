﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution08 : SolutionCommon
    {
        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var instructionList = new List<(string name, int offset)>();

            foreach (var line in inputLines)
            {
                // example lines: "acc +48" or "jmp -3"
                string[] nameAndOffset = line.Split(" ");
                string name = nameAndOffset[0];
                int offsetParsed = int.Parse(nameAndOffset[1].Replace("+", ""));
                instructionList.Add((name, offsetParsed));
            }

            int currentInstructionIndex = 0; // Starting point
            int accumulatorValue = 0;
            var visitedInstructions = new HashSet<int>() { currentInstructionIndex };
            bool secondOccurence = false;

            while (secondOccurence == false)
            {
                switch (instructionList[currentInstructionIndex].name)
                {
                    case "nop":
                        currentInstructionIndex += 1;
                        break;
                    case "acc":
                        accumulatorValue += instructionList[currentInstructionIndex].offset;
                        currentInstructionIndex += 1;
                        break;
                    case "jmp":
                        currentInstructionIndex += instructionList[currentInstructionIndex].offset;
                        break;
                };
                // HashSet's method Add() returns false if we try to add an element that is already included
                // We can use this to find the first time an instruction is repeated
                secondOccurence = !(visitedInstructions.Add(currentInstructionIndex));
            }

            return accumulatorValue.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            var instructionList = new List<(string name, int offset)>();

            foreach (var line in inputLines)
            {
                // acc +48
                string[] nameAndOffset = line.Split(" ");
                string name = nameAndOffset[0];
                int offsetParsed = int.Parse(nameAndOffset[1].Replace("+", ""));
                instructionList.Add((name, offsetParsed));
            }

            int accumulatorValue = 0;

            /*
            * We will naively try to "flip" every nop/jmp and check if the 
            * program completes. Since we end the for loop the moment we successfully
            * reach the end, we declare a break flag before the loop
            */

            bool successfulOperation = false;
            for (int i = 0; i < instructionList.Count; i++)
            {
                int currentInstructionIndex = 0; // We always start from 0
                accumulatorValue = 0;
                bool accDetected = false;
                bool corruptionDetected = false;
                HashSet<int> visitedInstructions = new HashSet<int>();

                switch (instructionList[i].name)
                {
                    case "acc":
                        accDetected = true;
                        break;
                    case "nop":
                        instructionList[i] = ("jmp", instructionList[i].offset);
                        break;
                    case "jmp":
                        instructionList[i] = ("nop", instructionList[i].offset);
                        break;
                }

                if (accDetected == true)
                {
                    continue;
                }

                visitedInstructions.Add(currentInstructionIndex);
                while (corruptionDetected == false && successfulOperation == false)
                {
                    switch (instructionList[currentInstructionIndex].name)
                    {
                        case "nop":
                            currentInstructionIndex += 1;
                            break;
                        case "acc":
                            accumulatorValue += instructionList[currentInstructionIndex].offset;
                            currentInstructionIndex += 1;
                            break;
                        case "jmp":
                            currentInstructionIndex += instructionList[currentInstructionIndex].offset;
                            break;
                    }
                    corruptionDetected = !(visitedInstructions.Add(currentInstructionIndex));
                    
                    if (currentInstructionIndex >= instructionList.Count)
                    {
                        successfulOperation = true;
                    }
                }

                if (successfulOperation == true)
                {
                    break;
                }
                else
                {
                    /*
                    * If we entered a loop, then the previous flip was not the one
                    * we are looking for. Flip back and try again.
                    */
                    switch (instructionList[i].name)
                    {
                        case "nop":
                            instructionList[i] = ("jmp", instructionList[i].offset);
                            break;
                        case "jmp":
                            instructionList[i] = ("nop", instructionList[i].offset);
                            break;
                    }
                }

            }

            return accumulatorValue.ToString();
        }
    }
}
