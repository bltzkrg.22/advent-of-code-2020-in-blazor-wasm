﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution06 : SolutionCommon
    {
        public override string PartA(string input)
        {
            int sumOfCounts = 0;

            // each groupForm in groupForms is a string with answers of multiple people
            // separated by a newline "character"
            string[] groupForms = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // Since in this part we are not interested in individual persons, but the
            // group as whole, we remove the newlines and analyze the result
            // character by character
            for (int i = 0; i < groupForms.Length; i++)
            {
                groupForms[i] = RemoveNewlinesFromString(groupForms[i]);
            }


            foreach (var groupForm in groupForms)
            {
                var allAnswers = new HashSet<char>();

                string[] personForms = groupForm.Split(new[] { "\r\n", "\r", "\n" },
                    StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

                foreach (var personForm in personForms)
                {
                    var personAnswers = new HashSet<char>();

                    foreach (var character in personForm)
                    {
                        personAnswers.Add(character);
                    }

                    allAnswers.UnionWith(personAnswers);
                }

                sumOfCounts += allAnswers.Count;
            }

            return sumOfCounts.ToString();


            string RemoveNewlinesFromString(string str)
            {
                return string.Join("", str.Split(new[] { "\r\n", "\r", "\n" },
                    StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries));
            }
        }

        public override string PartB(string input)
        {
            int sumOfCounts = 0;

            string[] groupForms = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var groupForm in groupForms)
            {
                var commonAnswers = new HashSet<char>
                    ( Enumerable.Range('a', 26).Select(x => (char)x) );

                string[] personForms = groupForm.Split(new[] { "\r\n", "\r", "\n" },
                    StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

                foreach (var personForm in personForms)
                {
                    var personAnswers = new HashSet<char>();

                    foreach (var character in personForm)
                    {
                        personAnswers.Add(character);
                    }

                    commonAnswers.IntersectWith(personAnswers);
                }

                sumOfCounts += commonAnswers.Count;
            }

            return sumOfCounts.ToString();
        }
    }
}
