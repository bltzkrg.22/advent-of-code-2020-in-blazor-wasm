﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution01 : SolutionCommon
    {
        private List<int> ParseAndSortInput(string input)
        {
            var dataValues = new List<int>();

            var reader = new StringReader(input);
            string currentLine;

            while ((currentLine = reader.ReadLine()) is not null)
            {
                dataValues.Add(Int32.Parse(currentLine));
            }

            dataValues.Sort();

            return dataValues;
        }

        public override string PartA(string input)
        {
            var numbers = ParseAndSortInput(input);

            for (int i = 0; i < numbers.Count; i++)
            {
                for (int j = i + 1; j < numbers.Count; j++)
                {
                    if (numbers[i] + numbers[j] > 2020)
                    {
                        break;
                    }
                    else if (numbers[i] + numbers[j] == 2020)
                    {
                        return (numbers[i] * numbers[j]).ToString();
                    }
                }
            }

            // If input was invalid and solution was not found, we return an error message.
            return "Solution not found!";
        }

        public override string PartB(string input)
        {
            var numbers = ParseAndSortInput(input);

            for (int i = 0; i < numbers.Count; i++)
            {
                for (int j = i + 1; j < numbers.Count; j++)
                {
                    if (numbers[i] + numbers[j] > 2020)
                    {
                        break;
                    }
                    for (int k = 0; k < numbers.Count; k++)
                    {
                        if (numbers[i] + numbers[j] + numbers[k] > 2020)
                        {
                            break;
                        }
                        else if (numbers[i] + numbers[j] + numbers[k] == 2020)
                        {
                            return (numbers[i] * numbers[j] * numbers[k]).ToString();
                        }
                    }
                }
            }
            // If input was invalid and solution was not found, we return an error message.
            return "Solution not found!";
        }
    }
}
