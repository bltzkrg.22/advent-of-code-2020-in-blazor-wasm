﻿## My solution

How do we validate a message against such a strange set of rules? We transform those rules into a regular expression, of course. This is definitely my most hacky solution in AoC 2020.

Consider the example input (but let’s drop the quotation marks):

```
0: 1 2
1: a
2: 1 3 | 3 1
3: b
```

Rule 1 is *simple*, i.e. it does not reference any other rules. We can replace all references to rule 1 with the content of rule 1, but let’s put it in parentheses:

```
0: (a) 2
2: (a) 3 | 3 (a)
3: b
```

Another simple rule is rule 3. Let’s do that again.

```
0: (a) 2
2: (a) (b) | (b) (a)
```

Now *rule 2* became a simple rule. It also becomes apparent that parentheses are necessary to correctly handle the alternation (the “vertical bar”).

```
0: (a) ((a) (b) | (b) (a))
```

Now we need to “sanitize” the rule by removing white spaces, an finally we get a regular expression:

```
(a)((a)(b)|(b)(a))
```

which we can use to test the messages.

-----

So, to solve **Part A**, i.e. to find the regular expression that is equivalent to rule 0, we will memorize all rules in a dictionary, iterate over it in a while loop:

- We find a simple rule.
- We replace all references to the simple rule with its actual content.
- We remove the simple rule from the dictionary.

In each step we remove a single rule (using method `RemoveNonCircularReferences`), so we keep iterating until rule 0 finally becomes a simple rule. The problem description states that

> [...] there are no loops in the rules [...]

so with valid input there *must* be a solution.

The regular expression constructed in that way will be an ugly, ugly mess of parentheses and vertical bars punctuated with occasional `a` or `b`. Fortunately, the regular expression engine doesn’t mind.

-----

**Part B** introduces an additional level of complexity by introducing two special looped rules:

```
8: 42 | 42 8
11: 42 31 | 42 11 31
```

However, instead of finding a general solution, we will just replace those rules with non-looped ones:

```
8: 42 | 42 42 | 42 42 42 | 42 42 42 42 … 
11: 42 31 | 42 42 31 31 | 42 42 42 31 31 31 | ...
```

Note that the each alternative is longer than the previous one; we can cutoff the rule evolution when any matching message would need to be longer than the maximal message length from the input data.

(In reality, I just set the `ruleRecurrenceDepth` constant to consecutive numbers and found the minimal length at which I stopped matching any additional messages).
