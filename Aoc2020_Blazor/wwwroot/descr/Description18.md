﻿## My solution

What’s the easiest way to parse the equations? Regular expressions, of course.

-----

We will analyze each equation “from inside out”. The following regular expression pattern (or more precisely, the second match group):

<pre><code><span style="color: var(--teal)">(.*?)</span><span style="color: var(--indigo)">\(</span><span style="color: var(--orange)">([^(]*?)</span><span style="color: var(--indigo)">\)</span><span style="color: var(--teal)">(.*?)$</span></code></pre>

matches *any string enclosed with parentheses, but which itself does not have any parentheses* – i.e. the contents between the deepest pair of parentheses. How does that work?

`(.*?)` is a catchall group for previous charactets. `\(` is the left bracket. `([^(]*?)` is a group of zero or more characters which are *not character `(`*. Then, `\)` is the right bracket. And finally `(.*?)$` is a catchall group for all characters that follow.

(All equations are assumed to be valid – i.e. with balanced parentheses; there is no error checking).

The contents of second match group do not have any parentheses, so it’s easy to calculate its value. In **Part A**, we can apply a second regular expression pattern:

```
(\d+) (\+|\*) (\d+)(.*?)$
```

Here, we catch two numbers `(\d+)` and an operator character between them. Then we can simply replace the first three match groups, i.e. `(\d+) (\+|\*) (\d+)`, with result of the appropriate operation, and copy the content of the final catchall group – repeat until there are no more operator characters in the string.

We calculated the value of in the most inner parentheses pair, so we can replace that sub-expression with its value – we just removed a pair of parentheses. We repeat the steps until there are no more parentheses in the expression – and then we can finally calculate the final value.

-----

The difference in **Part B** is that addition has a higher priority that multiplication. So instead of catching both operations with a single regular expression, we again find the most inner parentheses, but then look for addition first:

```
(.*?)(\d+) \+ (\d+)(.*?)$
```

and after the expression does not have `\+` characters anymore, we look for multiplication:

```
(\d+) \* (\d+)(.*?)$
```
