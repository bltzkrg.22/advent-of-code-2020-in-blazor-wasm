﻿## My solution

Day 23 is also very simple. All we need is a barebones implementation of a linked list. We don’t even need a doubly-linked list – we are only moving over the list in one direction!

Therefore, we construct such an array `nextCupLabel`:

```
int[] nextCupLabel = new int[maxCupLabel + 1];
```

so that `nextCupLabel[currentCup]` will be label of the cup directly clockwise from `currentCup`.

-----

This little trick:

```
int[] inputArray = input.Select(ch => ch - '0').ToArray();
```
reads the input string, which is a list of glued digits, e.g. `389547612`, and converts it to an array of corresponding integers.

-----

Some little details to note in the solution:

1\. Finding the insertion point: I implemented the requirement from problem description explicitly:

```
int destinationLabel = DecreaseWithWraparound(currentCup, maxCupLabel);
while (threeClockwiseCupLabels.Contains(destinationLabel))
{
    destinationLabel = DecreaseWithWraparound(destinationLabel, maxCupLabel);
}
```

\[…\]

```
int DecreaseWithWraparound(int number, int maxNumber)
{
    number--;
    return number <= 0 ? (maxNumber + number) : number;
}
```

But this assumes that there are no “unused” labels, i.e. all integers from 1 to the maximal label number are present. I wasn’t sure how to handle a case if a number was skipped; the input data never has such a skip. In the end the initial part of the solution does not use that assumption, and I just decided to leave it, even if the later part would break.

2\. While the operations we perform on the cups are the same in **Part A** and **Part B** are virtually the same, the expected answer is constructed differently. Hence the strange looking if statement in the code: `if (part == 'B')` \[…\]
