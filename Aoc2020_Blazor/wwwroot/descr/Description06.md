﻿## My solution

This day requires some basic knowledge of simple set operations: [union](https://en.wikipedia.org/wiki/Union_(set_theory)) and [intersection](https://en.wikipedia.org/wiki/Intersection_(set_theory))

First, we obviously need to split the input data on each double newline, to get the answers for each group.

To solve **Part A**, we need to find all questions where at least one person answered positively – i.e. count the number of characters that appeared at least once. Mathematically, if we treat answers of each person in the group as a set, we are looking for the *union* of all those sets. 

<img src="images/Set-Union.svg" alt="Set union" height="120">

So we create a hashset for each person in the group, and calculate their union. The result of that union will be the set with all questions, for which at least one person in group answered positively. We sum the counts of those hashsets to find the solution for **Part A**.

-----

In **Part B**, we need to find all questions where every person answered positively. So mathematically, this time we are looking for the *intersection* of sets.

<img src="images/Set-Intersection.svg" alt="Set intersection" height="120">

This piece of code:
```
var commonAnswers = new HashSet<char>
    ( Enumerable.Range('a', 26).Select(x => (char)x) );
```
uses LINQ to initalize a set with *all* possible answers, i.e. all 26 characters from a to z. If we intersect that set with the set of answers of person X, the result will be simply the set of answers of person X: `(All answers) ∩ (Answers X) = (Answers X)`. This initialization trick allows us to start the loop from the first person in a group, instead of initializing the value to the answers of first person and starting the loop from the second person.

Note that this approach will produce a garbage result if there could ever be a group with zero people!

As eariler, we create one final hashset for each group. To get the answer, we sum the counts of those hashsets.
