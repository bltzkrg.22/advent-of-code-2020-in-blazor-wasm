﻿## My solution

The goodbye problem is probably a little underwhelming, but I guess people have better things to do on Christmas Day.

Whole problem can be distilled to the following task:

*Find the (private) value of `doorLoopSize`, which after transforming number 7 returns value equal to (publicly known) `doorPublicKey`*.

This is a mathematically non-trivial problem – but the inputs are small enough that simply brute-forcing all posible values from 1 upwards will produce an answer in reasonable time (the answer must be lesser than 20201227).
