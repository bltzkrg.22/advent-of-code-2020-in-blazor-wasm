﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution11 : SolutionCommon
    {
        private readonly List<(int x, int y)> Directions = new List<(int x, int y)>()
            { (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1), (1,-1) };
        private const char EmptySpace = '.';
        private const char VacantSeat = 'L';
        private const char OccupiedSeat = '#';

        private const int toleranceA = 4;
        private const int toleranceB = 5;

        // I will want to mutate the "seat state" in every turn, on seat by seat basis.
        // An array of strings that I normally parse the input into is not pleasant to work with.
        private char[,] SeatMatrix;

        private void PopulateSeatMatrix(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            int numberOfRows = inputLines.Length;
            int numberOfCols = inputLines[0].Length;

            SeatMatrix = new char[numberOfCols, numberOfRows];

            for (int y = 0; y < numberOfRows; y++)
            {
                for (int x = 0; x < numberOfCols; x++)
                {
                    SeatMatrix[x, y] = inputLines[y][x];
                }
            }
        }

        private int CountOccupiedNeighboursA( int x, int y, char[,] seatMatrix)
        {
            int counter = 0;

            int numberOfRows = SeatMatrix.GetLength(1);
            int numberOfCols = SeatMatrix.GetLength(0);

            foreach (var direction in Directions)
            {
                int neighbourX = x + direction.x;
                int neighbourY = y + direction.y;
                char neighbourStatus;

                if (neighbourX < 0 || neighbourX >= numberOfCols || neighbourY < 0 || neighbourY >= numberOfRows)
                {
                    continue;
                }

                neighbourStatus = seatMatrix[neighbourX, neighbourY];
                if (neighbourStatus == OccupiedSeat)
                {
                    counter++;
                }
            }

            return counter;
        }

        private int CountOccupiedNeighboursB(int x, int y, char[,] seatMatrix)
        {
            int counter = 0;

            int numberOfRows = SeatMatrix.GetLength(1);
            int numberOfCols = SeatMatrix.GetLength(0);

            foreach (var direction in Directions)
            {
                char neighbourStatus = 'N'; // initialize to some garbage value

                // Initialize the variables for the while loop
                bool edgeOrNeighbourFound = false;
                int neighbourX = x;
                int neighbourY = y;

                while (!edgeOrNeighbourFound)
                {
                    // Make a step in current direction
                    neighbourX += direction.x;
                    neighbourY += direction.y;
                    if (neighbourX < 0 || neighbourX >= numberOfCols || neighbourY < 0 || neighbourY >= numberOfRows)
                    {
                        // Edge found
                        edgeOrNeighbourFound = true;
                        neighbourStatus = EmptySpace;
                    }
                    else if (seatMatrix[neighbourX, neighbourY] != EmptySpace)
                    {
                        // Non-empty neighbour found
                        edgeOrNeighbourFound = true;
                        neighbourStatus = seatMatrix[neighbourX, neighbourY];
                    }
                    else
                    {
                        // Do nothing
                    }
                }

                if (neighbourStatus == OccupiedSeat)
                {
                    counter++;
                }
            }

            return counter;
        }

        public override string PartA(string input)
        {
            PopulateSeatMatrix(input);
            int tolerance = toleranceA;

            int numberOfRows = SeatMatrix.GetLength(1);
            int numberOfCols = SeatMatrix.GetLength(0);

            bool flipOccurred = true;
            while (flipOccurred)
            {
                char[,] nextSeatMatrix = new char[numberOfCols, numberOfRows];
                flipOccurred = false;
                for (int x = 0; x < numberOfCols; x++)
                {
                    for (int y = 0; y < numberOfRows; y++)
                    {
                        // Do not waste time to compute CountOccupiedNeighboursA for empty space
                        if (SeatMatrix[x, y] == EmptySpace)
                        {
                            nextSeatMatrix[x, y] = SeatMatrix[x, y];
                            continue;
                        }

                        int occupiedNeighbours = CountOccupiedNeighboursA(x, y, SeatMatrix);

                        if (occupiedNeighbours > 0 && occupiedNeighbours < tolerance)
                        {
                            nextSeatMatrix[x, y] = SeatMatrix[x, y];
                        }
                        else if (SeatMatrix[x,y] == VacantSeat && occupiedNeighbours == 0)
                        {
                            nextSeatMatrix[x, y] = OccupiedSeat;
                            flipOccurred = true;
                        }
                        else if (SeatMatrix[x, y] == OccupiedSeat && occupiedNeighbours >= tolerance)
                        {
                            nextSeatMatrix[x, y] = VacantSeat;
                            flipOccurred = true;
                        }
                        else
                        {
                            nextSeatMatrix[x, y] = SeatMatrix[x, y];
                        }
                    }
                }


                SeatMatrix = nextSeatMatrix;
            }

            int finalOccupiedCounter = 0;

            for (int x = 0; x < numberOfCols; x++)
            {
                for (int y = 0; y < numberOfRows; y++)
                {
                    if (SeatMatrix[x,y] == OccupiedSeat)
                    {
                        finalOccupiedCounter++;
                    }
                }
            }

            return finalOccupiedCounter.ToString();
        }

        public override string PartB(string input)
        {
            PopulateSeatMatrix(input);
            int tolerance = toleranceB;

            int numberOfRows = SeatMatrix.GetLength(1);
            int numberOfCols = SeatMatrix.GetLength(0);

            bool flipOccurred = true;
            while (flipOccurred)
            {
                char[,] nextSeatMatrix = new char[numberOfCols, numberOfRows];
                flipOccurred = false;
                for (int x = 0; x < numberOfCols; x++)
                {
                    for (int y = 0; y < numberOfRows; y++)
                    {
                        // Do not waste time to compute CountOccupiedNeighboursA for empty space
                        if (SeatMatrix[x, y] == EmptySpace)
                        {
                            nextSeatMatrix[x, y] = SeatMatrix[x, y];
                            continue;
                        }

                        int occupiedNeighbours = CountOccupiedNeighboursB(x, y, SeatMatrix);

                        if (occupiedNeighbours > 0 && occupiedNeighbours < tolerance)
                        {
                            nextSeatMatrix[x, y] = SeatMatrix[x, y];
                        }
                        else if (SeatMatrix[x, y] == VacantSeat && occupiedNeighbours == 0)
                        {
                            nextSeatMatrix[x, y] = OccupiedSeat;
                            flipOccurred = true;
                        }
                        else if (SeatMatrix[x, y] == OccupiedSeat && occupiedNeighbours >= tolerance)
                        {
                            nextSeatMatrix[x, y] = VacantSeat;
                            flipOccurred = true;
                        }
                        else
                        {
                            nextSeatMatrix[x, y] = SeatMatrix[x, y];
                        }
                    }
                }


                SeatMatrix = nextSeatMatrix;
            }

            int finalOccupiedCounter = 0;

            for (int x = 0; x < numberOfCols; x++)
            {
                for (int y = 0; y < numberOfRows; y++)
                {
                    if (SeatMatrix[x, y] == OccupiedSeat)
                    {
                        finalOccupiedCounter++;
                    }
                }
            }

            return finalOccupiedCounter.ToString();
        }
    }
}
