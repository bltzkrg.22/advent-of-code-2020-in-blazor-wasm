﻿using System;
using System.Collections.Generic;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution05 : SolutionCommon
    {
        // input pattern: `BFFFFFFLLR` 7 letters for row number, 3 letters for column number
        private const int RowBits = 7;
        private const int ColBits = 3;

        private readonly HashSet<char> Ones = new HashSet<char>() { 'B', 'R' };
        // private readonly HashSet<char> Zeroes = new HashSet<char>() { 'F', 'L' };

        private int DecodeBoardingPass(string pass)
        {
            int answer = 0;

            for (int bit = 0; bit < RowBits + ColBits; bit++)
            {
                if (Ones.Contains(pass[bit]))
                {
                    answer += 1 << ((RowBits + ColBits - 1) - bit);
                }
            }

            return answer;
        }

        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            int maxBoardingPassNumber = Int32.MinValue;

            foreach (string boardingPass in inputLines)
            {
                int boardingPassNumber = DecodeBoardingPass(boardingPass);
                if (boardingPassNumber > maxBoardingPassNumber)
                {
                    maxBoardingPassNumber = boardingPassNumber;
                }
            }

            return maxBoardingPassNumber.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            int maxBoardingPassNumber = Int32.MinValue;
            int minBoardingPassNumber = Int32.MaxValue;
            int sumOfPassNumbers = 0;
            int myPassNumber = -1;

            foreach (string boardingPass in inputLines)
            {
                int boardingPassNumber = DecodeBoardingPass(boardingPass);
                sumOfPassNumbers += boardingPassNumber;

                if (boardingPassNumber > maxBoardingPassNumber)
                {
                    maxBoardingPassNumber = boardingPassNumber;
                }
                if (boardingPassNumber < minBoardingPassNumber)
                {
                    minBoardingPassNumber = boardingPassNumber;
                }

                myPassNumber = (maxBoardingPassNumber - minBoardingPassNumber + 1) *
                        (maxBoardingPassNumber + minBoardingPassNumber) / 2 - sumOfPassNumbers;
            }

            return myPassNumber.ToString();
        }
    }
}
