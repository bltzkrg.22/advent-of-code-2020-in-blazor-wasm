﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Aoc2020_Blazor.Solutions
{
    public class Solution21 : SolutionCommon
    {
        // mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
        private const string linePattern = @"^(.*?) \(contains (.*?)\)$";

        private void GetIngredientsAndAllergens(string[] inputLines, HashSet<string> ingredients, HashSet<string> allergens)
        {
            foreach (var line in inputLines)
            {
                // mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
                string[] processedLine = Regex.Split(line, linePattern);

                foreach (var ingredient in processedLine[1].Split(" ", StringSplitOptions.TrimEntries))
                {
                    ingredients.Add(ingredient);
                }
                foreach (var allergen in processedLine[2].Split(",", StringSplitOptions.TrimEntries))
                {
                    allergens.Add(allergen);
                }
            }
        }

        private void PossibleIngredientToAllegrenMatches(string[] inputLines, HashSet<string> ingredients, HashSet<string> allergens,
            Dictionary<string, HashSet<string>> possibleAllergenMatches)
        {
            foreach (var ingredientX in ingredients)
            {
                possibleAllergenMatches[ingredientX] = new HashSet<string>();
                possibleAllergenMatches[ingredientX].UnionWith(allergens);

                Regex ingredientXRegex = new Regex(@"\b" + ingredientX + @"\b");

                foreach (var allergenY in possibleAllergenMatches[ingredientX])
                {
                    Regex allergenYRegex = new Regex(@"\b" + allergenY + @"\b");

                    foreach (var line in inputLines)
                    {
                        // Attention! some ingredient names are substrings
                        // of other names, like kl => kljktfm, therefore
                        // simple string.Contains() gives false positives :/
                        if (allergenYRegex.IsMatch(line) && !ingredientXRegex.IsMatch(line))
                        {
                            possibleAllergenMatches[ingredientX].Remove(allergenY);
                            break;
                        }
                    }
                }
            }
        }


        public override string PartA(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            HashSet<string> ingredients = new HashSet<string>();
            HashSet<string> allergens = new HashSet<string>();
            List<List<string>> productIngredientList = new List<List<string>>();

            Dictionary<string, HashSet<string>> possibleAllergenMatches = new Dictionary<string, HashSet<string>>();

            GetIngredientsAndAllergens(inputLines, ingredients, allergens);

            // Can ingerdient X contain allergen Y?
            // i.e.: Does every product that contains allergen Y include ingredient X?
            PossibleIngredientToAllegrenMatches(inputLines, ingredients, allergens, possibleAllergenMatches);


            int answerCounter = 0;
            int inertCounter = 0;

            foreach (var ingredientX in ingredients.Where(x => possibleAllergenMatches[x].Count == 0))
            {
                inertCounter++;
                // subcounter => How many times does the inert ingredient from current loop iteration is found
                int subcounter = 0;

                foreach (var line in inputLines)
                {
                    Regex ingredientXRegex = new Regex(@"\b" + ingredientX + @"\b");
                    if (ingredientXRegex.IsMatch(line))
                    {
                        subcounter++;
                    }

                }
                answerCounter += subcounter;
            }

            return answerCounter.ToString();
        }

        public override string PartB(string input)
        {
            string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            HashSet<string> ingredients = new HashSet<string>();
            HashSet<string> allergens = new HashSet<string>();
            List<List<string>> productIngredientList = new List<List<string>>();

            Dictionary<string, HashSet<string>> possibleAllergenMatches = new Dictionary<string, HashSet<string>>();

            GetIngredientsAndAllergens(inputLines, ingredients, allergens);

            // Can ingerdient X contain allergen Y?
            // i.e.: Does every product that contains allergen Y include ingredient X?
            PossibleIngredientToAllegrenMatches(inputLines, ingredients, allergens, possibleAllergenMatches);

            // Discard all ingredients that cannot be matched to any allegrens
            foreach (var ingredientX in ingredients)
            {
                if (possibleAllergenMatches[ingredientX].Count == 0)
                {
                    possibleAllergenMatches.Remove(ingredientX);
                }
            }

            // Key = allegren, Value = ingredient. We use such order, because the answer is supposed 
            // to be a list of ingerdients *sorted by* matching allergen names!
            // By using a SortedDictionary we get the correct order of ingredients "naturally" while
            // populating the matchedPairs dictionary.
            var matchedPairs = new SortedDictionary<string, string>();

            while (possibleAllergenMatches.Count > 0)
            {
                // In each loop iteration we find an ingredient, that can be matched to only one
                // allegren. We save this pair, and remove the matched allergen from
                // the possible matches for other ingredients.
                var pair = possibleAllergenMatches.Where(element => element.Value.Count == 1).First();
                matchedPairs.Add(pair.Value.First(), pair.Key);

                possibleAllergenMatches.Remove(pair.Key);

                foreach (var dictEntry in possibleAllergenMatches)
                {
                    dictEntry.Value.Remove(pair.Value.First());
                }
            }

            // answer is a comma-separated list of dangerous ingredients
            string answer = String.Join(",", matchedPairs.Select(kvpair => kvpair.Value).ToArray());

            return answer;
        }
    }
}
